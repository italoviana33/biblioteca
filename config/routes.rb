Rails.application.routes.draw do
  resources :reservations
  resources :books
  resources :librarians
  resources :clients
  resources :authors
  get 'home/index'

  root 'home#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
