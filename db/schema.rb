# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_22_182159) do

  create_table "authors", force: :cascade do |t|
    t.string "Name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "books", force: :cascade do |t|
    t.string "Name"
    t.integer "Author_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["Author_id"], name: "index_books_on_Author_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "Name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "librarians", force: :cascade do |t|
    t.string "Name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.boolean "Active"
    t.integer "book_id", null: false
    t.integer "client_id", null: false
    t.integer "librarian_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["book_id"], name: "index_reservations_on_book_id"
    t.index ["client_id"], name: "index_reservations_on_client_id"
    t.index ["librarian_id"], name: "index_reservations_on_librarian_id"
  end

  add_foreign_key "books", "Authors"
  add_foreign_key "reservations", "books"
  add_foreign_key "reservations", "clients"
  add_foreign_key "reservations", "librarians"
end
