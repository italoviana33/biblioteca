json.extract! author, :id, :Name, :created_at, :updated_at
json.url author_url(author, format: :json)
