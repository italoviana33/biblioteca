json.extract! librarian, :id, :Name, :created_at, :updated_at
json.url librarian_url(librarian, format: :json)
